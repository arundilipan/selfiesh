//
//  CustomCamera.h
//  Selfiesh
//
//  Created by Arun Dilipan on 2/3/15.
//
//

#import <Cordova/CDV.h>
#import "CustomCameraViewController.h"

@interface CustomCamera : CDVPlugin
-(void) openCamera:(CDVInvokedUrlCommand*)command;

-(void) capturedImageWithPath:(NSString*)imagePath;
@property (strong, nonatomic) CustomCameraViewController* overlay;
@property (strong, nonatomic) CDVInvokedUrlCommand* latestCommand;
@property (readwrite, assign) BOOL hasPendingOperation;
@end
