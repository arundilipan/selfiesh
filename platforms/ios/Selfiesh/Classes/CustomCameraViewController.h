//
//  CustomCameraViewController.h
//  Selfiesh
//
//  Created by Arun Dilipan on 2/3/15.
//
//

#import <UIKit/UIKit.h>

@class CustomCamera;

@interface CustomCameraViewController : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

// Action method
-(IBAction) takePhotoButtonPressed:(id)sender forEvent:(UIEvent*)event;

// Declare some properties (to be explained soon)
@property (strong, nonatomic) CustomCamera* plugin;
@property (strong, nonatomic) UIImagePickerController* picker;

@end
