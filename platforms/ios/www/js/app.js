// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngStorage', 'oauth', 'react', 'ngCookies', 'LocalStorageModule', 'ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($httpProvider, localStorageServiceProvider, $ionicConfigProvider) {
  $httpProvider.defaults.withCredentials = true;
  localStorageServiceProvider.setPrefix('selfiesh');
  $ionicConfigProvider.tabs.position('bottom')
})


.run(function($http, $cookies){
  $http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;
})

.provider('myCSRF',[function(){
  var headerName = 'X-CSRFToken';
  var cookieName = 'csrftoken';
  var allowedMethods = ['GET', 'POST'];

  this.setHeaderName = function(n) {
    headerName = n;
  }
  this.setCookieName = function(n) {
    cookieName = n;
  }
  this.setAllowedMethods = function(n) {
    allowedMethods = n;
  }
  this.$get = ['$cookies', function($cookies){
    return {
      'request': function(config) {
        if(allowedMethods.indexOf(config.method) === -1) {
          // do something on success
          config.headers[headerName] = $cookies[cookieName];
        }
        return config;
      }
    }
  }];
  this.$post = ['$cookies', function($cookies){
    return {
      'request': function(config) {
        if(allowedMethods.indexOf(config.method) === -1) {
          // do something on success
          config.headers[headerName] = $cookies[cookieName];
        }
        return config;
      }
    }
  }];
}]).config(function($httpProvider) {
  $httpProvider.interceptors.push('myCSRF');
})


.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider



  // setup an abstract state for the tabs directive
  .state('tab', {
    url: "/tab",
    abstract: true,
    templateUrl: "templates/tabs.html"
  })

  .state('welcome', {
    url: '/welcome',
    views: {
      '': {
        templateUrl: "templates/welcome.html",
        controller: "WelcomeCtrl"
      }
    }
  })

  .state('login_facebook', {
    url: "/login/facebook",
    views: {
      '': {
        templateUrl: 'templates/login_facebook.html'
      }
    }
  })

  .state('login_selfiesh', {
    url: "/login/selfiesh",
    views: {
      '': {
        templateUrl: 'templates/login_selfiesh.html',
        controller: "SelfieshLoginCtrl"
      }
    }
  })

  .state('signup', {
    url: "/signup",
    views: {
      '': {
        templateUrl: 'templates/signup.html',
        controller: 'SignupCtrl'
      }
    }
  })

  .state('start', {
    url: "/start",
    views: {
      '': {
        controller: 'StartCtrl'
      }
    }
  })



  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.post', {
    url: '/dash/:postId',
    views: {
      'tab-dash': {
        templateUrl: 'template/tab-upload.html'
      }
    }
  })

  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'ChatsCtrl'
        }
      }
    })
    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })


  .state('tab.friends', {
      url: '/friends',
      views: {
        'tab-friends': {
          templateUrl: 'templates/tab-friends.html',
          controller: 'FriendsCtrl'
        }
      }
    })
  .state('tab.friend-detail', {
    url: '/friend/:friendId',
    views: {
      'tab-friends': {
        templateUrl: 'templates/friend-detail.html',
        controller: 'FriendDetailCtrl'
      }
    }
  })

  .state('tab.camera', {
    url: '/camera',
    views: {
      'tab-camera': {
        templateUrl: 'templates/tab-camera.html',
        controller: 'CameraCtrl'
      }
    }
  })

  .state('tab.posts', {
    url: '/posts',
    views: {
      'tab-posts': {
        templateUrl: 'templates/tab-posts.html',
        controller: 'PostsCtrl'
      }
    }
  })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })

  .state('tab.download-photos', {
    url: "/account/download-photos",
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-download-photos.html'
      }
    }
  })

  .state('tab.link-accounts', {
    url: '/account/link-all',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-link-account.html'
      }
    }
  })


  ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dash');
});
