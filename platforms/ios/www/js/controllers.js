angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $ionicModal) {
  
  $ionicModal.fromTemplateUrl('templates/image-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.showModal = function (evt) {
    $scope.imgSrc = evt.target.src;
    $scope.openModal();
  }

  $scope.openModal = function() {
    $scope.modal.show();
  };

  $scope.closeModal = function() {
    $scope.modal.hide();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hide', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
  $scope.$on('modal.shown', function() {
    console.log('Modal is shown!');
  });
})

.controller('ChatsCtrl', function($scope, Chats) {
  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  }
})

.controller('CameraCtrl', function($scope, $cordovaCamera, $rootScope, $location, $selfieshCamera, $cordovaFile) {
  $scope.capture = function () {
    document.addEventListener("deviceready", function () {
      $selfieshCamera.takePicture(function(image) {
        var img = image;

        document.getElementById("myImage").src = "file://" + image.url;

        $scope.message = image;
      }, function(error) {
        $scope.message = error;
      });
    }, false);
  };
  
  
})

.controller('WelcomeCtrl', function($scope, $location) {
  $scope.switchToLogin = function() {
    $location.path("/login/selfiesh");
  };

  $scope.switchToSignup = function() {
    $location.path("/signup")
  }
})

.controller('StartCtrl', function($scope, $location, localStorageService){
  if (localStorageService.get("currentUser")) {
    $location.path("/tab/dash");
  } else {
    $location.path("/welcome")
  }
})

.controller('SelfieshLoginCtrl', function($scope, $http, $location, localStorageService, $cordovaProgress) {
  $scope.user = {};
  $scope.login = function() {
    $cordovaProgress.showSimple(true);

    $http.post("http://www.selfiesh.in/login", {}, {
      params: {
        username: $scope.user.username, 
        password: $scope.user.password
      }
    })

    .success(function (data, status, header, config) {
      $cordovaProgress.hide();
      if (data.error === undefined) {
        localStorageService.set('currentUser', data.username);
        $location.path("/tab/dash");
      } else {
        $scope.error = data.error;
      }
    })

    .error(function (data, status, header, config) {
      $cordovaProgress.hide();
      $scope.error = data;
    });
  }
})

.controller('SignupCtrl', function($scope, $http, $location) {
  $scope.user = {};
  $scope.signUp = function() {
    $http.post("http://www.selfiesh.in/signup", {}, {
      params: {
        username: $scope.user.username,
        password: $scope.user.password,
        email: $scope.user.email
      }
    })

    .success(function (data, status, header, config) {
      if (data.error === undefined) {
        $location.path("/login/selfiesh");
      } else {
        $scope.error = data.error;
      }
    })

    .error(function (data, status, header, config) {
      $scope.error = data;
    });
  }
})

.controller('LoginCtrl', function($scope) {
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('FriendsCtrl', function($scope, Friends) {
  $scope.friends = Friends.all();
})

.controller('FriendDetailCtrl', function($scope, $stateParams, Friends) {
  $scope.friend = Friends.get($stateParams.friendId);
})

.controller('PostsCtrl', function($scope, $ionicModal) {
  $ionicModal.fromTemplateUrl('templates/image-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.showModal = function (evt) {
    $scope.imgSrc = evt.target.src;
    $scope.openModal();
  }

  $scope.openModal = function() {
    $scope.modal.show();
  };

  $scope.closeModal = function() {
    $scope.modal.hide();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hide', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
  $scope.$on('modal.shown', function() {
    console.log('Modal is shown!');
  });
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
