package com.selfiesh.CameraApp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import com.ionicframework.selfiesh828808.R;
import android.util.Base64;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class CameraActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    private CameraPreview mCameraPreview;
    public static Camera camera;
    protected static int rotation;
    public static String mCurrentPhotoPath;
    public static Uri mCurrentUri;

    public static CameraActivity currentActivity = null;

    public void onClick(View v) {
        camera.takePicture(null, null, mPictureCallback);
    }
    public static byte[] currentImageData = null;

    public byte[] resizeImage(byte[] input) {
        Bitmap original = Bitmap.createBitmap(1920, 1080, Bitmap.Config.ARGB_8888);
        original.copyPixelsFromBuffer(ByteBuffer.wrap(input));

        ByteArrayOutputStream blob = new ByteArrayOutputStream();
        original.compress(Bitmap.CompressFormat.PNG, 100, blob);

        return blob.toByteArray();
    }

    public static Camera.PictureCallback mPictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File pictureFile = null;
            try {
                pictureFile = createImageFile();
                if (pictureFile == null) {
                    Log.d("Error:", "check storage permissions");
                    return;
                }
            } catch (IOException e) {
                Log.d("Error:", "check storage permissions");
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);

                fos.close();

            } catch (NullPointerException e) {
                Log.e("Error:", "NullPointerException: " + e.getMessage());
            } catch (Exception e) {
                Log.e("Error:", "File not found or error accessing file " + e.getMessage());
            }



            currentImageData = data;

            currentActivity.finish();
        }
    };

    private static File createImageFile() throws IOException {
        // Create an image file name

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "PNG_" + timeStamp + "_";
        File storageDir = currentActivity.getApplicationContext().getCacheDir();
        if (!storageDir.exists()) storageDir.mkdir();
        File image = File.createTempFile(imageFileName, ".png", storageDir);

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getName();
        return image;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        currentActivity = this;

        rotation = getWindowManager().getDefaultDisplay().getRotation();

        try {
            camera = Camera.open(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mCameraPreview = new CameraPreview(this);

        setContentView(R.layout.main);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            if (camera == null) {
                camera = Camera.open(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void rotateImage(String file) throws IOException {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        Bitmap bm = BitmapFactory.decodeFile(file, opts);

//        Bitmap scaled = Bitmap.createBitmap(bm, 0, 0, CameraPreview.width, CameraPreview.height);

        Matrix matrix = new Matrix();
        matrix.postRotate(180);
        matrix.setScale(-1, 1);
//        matrix.postTranslate(0, CameraPreview.height - bm.getWidth());

//        Bitmap rotatedBitmap = Bitmap.createBitmap(scaled, 0, 0, scaled.getWidth(), scaled.getHeight(), matrix, true);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bm, Math.abs(bm.getWidth() - CameraPreview.width) /2, 0, CameraPreview.width, CameraPreview.height, matrix, true);


        FileOutputStream out = new FileOutputStream(file);
        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
        out.close();
    }
}
