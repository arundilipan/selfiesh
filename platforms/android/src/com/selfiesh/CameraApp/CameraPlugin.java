package com.selfiesh.CameraApp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.util.Log;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by arun on 2/10/15.
 */
public class CameraPlugin extends CordovaPlugin {
    private static final int DATA_URL = 0; // Return base64 encoded string
    private static final int FILE_URI = 1; // Return file uri (content://media/external/images/media/2 for Android)
    private static final int NATIVE_URI = 2; // On Android, this is the same as FILE_URI
    private static final int PHOTOLIBRARY = 0; // Choose image from picture library (same as SAVEDPHOTOALBUM for Android)
    private static final int CAMERA = 1; // Take picture from camera
    private static final int SAVEDPHOTOALBUM = 2; // Choose image from picture library (same as PHOTOLIBRARY for Android)
    private static final int PICTURE = 0; // allow selection of still pictures only. DEFAULT. Will return format specified via DestinationType
    private static final int VIDEO = 1; // allow selection of video only, ONLY RETURNS URL
    private static final int ALLMEDIA = 2; // allow selection from all media types
    private static final int JPEG = 0; // Take a picture of type JPEG
    private static final int PNG = 1; // Take a picture of type PNG
    private static final String GET_PICTURE = "Get Picture";
    private static final String GET_VIDEO = "Get Video";
    private static final String GET_All = "Get All";
    private static final String LOG_TAG = "CameraLauncher";
    private static final int CROP_CAMERA = 100;

    public CallbackContext context = null;
    public byte[] data = null;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        if (action.equals("takePicture")) {
            return takePicture(callbackContext, FILE_URI);
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        data = intent.getByteArrayExtra("data");
    }



    public boolean takePicture(final CallbackContext callbackContext, int returnType) {
        cordova.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                context = callbackContext;
                Context cameraContext = cordova.getActivity().getApplicationContext();
                Intent intent = new Intent(cameraContext, CameraActivity.class);
                cordova.getActivity().startActivityForResult(intent, 1);
            }
        });

        while (CameraActivity.currentImageData == null) {

        }

        JSONObject obj = new JSONObject();
        try {
            String loc = cordova.getActivity().getApplicationContext().getCacheDir().getAbsolutePath() + "/" + CameraActivity.mCurrentPhotoPath;

            obj.put("width", CameraPreview.width);
            obj.put("height", CameraPreview.height);
            obj.put("url", loc);

            CameraActivity.rotateImage(loc);


            callbackContext.success(obj);
        } catch (JSONException e) {
            callbackContext.success("error");
        } catch (NullPointerException e) {
            Log.d("Error:", "bitmap could not be read.");
        } catch (Exception e) {
            Log.d("Error:", "bitmap could not be read.");
        }


        return true;
    }


}
