var exec = require('cordova');

exports.takePicture = function(success, error) {
    exec(success, error, "com.selfiesh.CameraApp.CameraPlugin", "takePicture", []);
};