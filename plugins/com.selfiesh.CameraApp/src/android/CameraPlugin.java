package com.selfiesh.CameraApp;

import android.content.Context;
import android.content.Intent;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;

/**
 * Created by arun on 2/10/15.
 */
public class CameraPlugin extends CordovaPlugin {
    private static final int DATA_URL = 0; // Return base64 encoded string
    private static final int FILE_URI = 1; 
    private static final int NATIVE_URI = 2; // On Android, this is the same as FILE_URI
    private static final int PHOTOLIBRARY = 0; 
    private static final int CAMERA = 1; // Take picture from camera
    private static final int SAVEDPHOTOALBUM = 2; 
    private static final int PICTURE = 0; // allow selection of still pictures only. DEFAULT. Will return format specified via DestinationType
    private static final int VIDEO = 1; // allow selection of video only, ONLY RETURNS URL
    private static final int ALLMEDIA = 2; // allow selection from all media types
    private static final int JPEG = 0; // Take a picture of type JPEG
    private static final int PNG = 1; // Take a picture of type PNG
    private static final String GET_PICTURE = "Get Picture";
    private static final String GET_VIDEO = "Get Video";
    private static final String GET_All = "Get All";
    private static final String LOG_TAG = "CameraLauncher";
    private static final int CROP_CAMERA = 100;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        return takePicture(callbackContext);
    }

    public boolean takePicture(CallbackContext callbackContext) {
        // cordova.getActivity().runOnUiThread(new Runnable() {
        //     @Override
        //     public void run() {
        //         Context cameraContext = cordova.getActivity().getApplicationContext();
        //         Intent intent = new Intent(cameraContext, CameraActivity.class);
        //         cordova.getActivity().startActivity(intent);
        //     }
        // });

        Context cameraContext = cordova.getActivity().getApplicationContext();
        Intent intent = new Intent(cameraContext, CameraActivity.class);
        cordova.startActivityForResult(this, intent, (CAMERA + 1) * 16 + FILE_URI + 1);

        callbackContext.success("Success!");

        return true;   
    }
}
