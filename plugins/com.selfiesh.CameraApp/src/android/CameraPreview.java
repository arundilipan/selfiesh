package com.selfiesh.CameraApp;

import android.content.Context;
import android.hardware.Camera;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by arun on 2/8/15.
 */
/** A basic Camera preview class */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder mHolder;





    public CameraPreview(Context context) {
        super(context);

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public CameraPreview(Context context, AttributeSet attrs) {
        super(context, attrs);

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }


    public void surfaceCreated(SurfaceHolder holder) {
        // The Surface has been created, now tell the camera where to draw the preview.
        try {
            CameraActivity.camera.setPreviewDisplay(holder);
            CameraActivity.camera.startPreview();
            CameraActivity.camera.setDisplayOrientation(90);
            Camera.Parameters params = CameraActivity.camera.getParameters();
            int width = getHeight();
            int height = getWidth();
            params.setSceneMode(Camera.Parameters.SCENE_MODE_PORTRAIT);
            params.setPreviewSize(width, height);
            CameraActivity.camera.setParameters(params);
        } catch (IOException e) {
            Log.d("Error:", "Error setting camera preview: " + e.getMessage());
        } catch (NullPointerException e) {
            Log.d("Error:", "Couldn't init camera.");
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (CameraActivity.camera != null) {
            CameraActivity.camera.stopPreview();
            CameraActivity.camera.setPreviewCallback(null);
            CameraActivity.camera.release();
            CameraActivity.camera = null;
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        if (mHolder.getSurface() == null) {
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            CameraActivity.camera.stopPreview();
        } catch (Exception e){
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings
        try {
            CameraActivity.camera.setPreviewDisplay(mHolder);
            CameraActivity.camera.startPreview();

        } catch (Exception e){
            Log.d("Error:", "Error starting camera preview: " + e.getMessage());
        }
    }
}