package com.selfiesh.CameraApp;

import android.app.Activity;
import android.hardware.Camera;
import android.media.Image;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CameraActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    private CameraPreview mCameraPreview;
    public static Camera camera;
    protected static int rotation;
    public static String mCurrentPhotoPath;

    public interface CameraActivityListener {
        public void onPictureTaken(String currentPhotoPath);
    }

    public void onClick(View v) {
        camera.takePicture(null, null, mPictureCallback);
    }

    public static Camera.PictureCallback mPictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File pictureFile = null;
            try {
                pictureFile = createImageFile();
                if (pictureFile == null) {
                    Log.d("Error:", "check storage permissions");
                    return;
                }
            } catch (IOException e) {
                Log.d("Error:", "check storage permissions");
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (NullPointerException e) {
                Log.e("Error:", "NullPointerException: " + e.getMessage());
            } catch (Exception e) {
                Log.e("Error:", "File not found or error accessing file " + e.getMessage());
            }
        }
    };

    private static File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rotation = getWindowManager().getDefaultDisplay().getRotation();

        try {
            camera = Camera.open(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mCameraPreview = new CameraPreview(this);

        setContentView(R.layout.main);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            if (camera == null) {
                camera = Camera.open(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
